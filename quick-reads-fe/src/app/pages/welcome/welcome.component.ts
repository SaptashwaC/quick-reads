import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(private router: Router) {
    // router.events.subscribe((url:any) => console.log(url));
      console.log(router.url);
  }

  ngOnInit(): void {
    console.log('Welcome')
  }

}
