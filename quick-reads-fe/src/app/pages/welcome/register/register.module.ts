import { NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import { CoreModule } from 'src/app/core/core.module';



@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    RegisterRoutingModule,
    CoreModule
  ],
  exports:[RegisterComponent]
})
export class RegisterModule { }
