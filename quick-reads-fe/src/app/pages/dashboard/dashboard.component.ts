import { Component, OnInit } from '@angular/core';
import { SidenavSettingsConst } from 'src/app/shared/constants/settings';
import { SideNavSettings } from 'src/app/shared/interfaces/sidenav-settings.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }
  sideNavSettings: SideNavSettings = {mode: SidenavSettingsConst.DRAWER_MODE_VALUE, hasBackDrop: SidenavSettingsConst.DRAWER_BACKDROP_VALUE};
  panelOpenState = false;
  ngOnInit(): void {
  }

}
