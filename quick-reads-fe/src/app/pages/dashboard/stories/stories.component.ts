import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import * as M from 'materialize-css';


@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss'],
})
export class StoriesComponent implements OnInit {

  constructor() { }

  options = { fullWidth: false };
  // items = [{ img: 'https://picsum.photos/200/200?random=1' },
  // { img: 'https://picsum.photos/200/200?random=2' },
  // { img: 'https://picsum.photos/200/200?random=3' },
  // { img: 'https://picsum.photos/200/200?random=4' },
  // { img: 'https://picsum.photos/200/200?random=5' },
  // { img: 'https://picsum.photos/200/200?random=6' },
  // { img: 'https://picsum.photos/200/200?random=7' },
  // { img: 'https://picsum.photos/200/200?random=8' }
  // ];

  // hrefs = ['one', 'two', 'three', 'four', 'five'];


  ngOnInit() {
    // ERROR
    // Error: Cannot read property 'clientWidth' of undefined
    // let elems = document.querySelectorAll('.carousel');
    // let instances = M.Carousel.init(elems, this.options);
    
  }

  ngAfterViewInit() {
    // no errors
    var elems = document.querySelectorAll('.fixed-action-btn');
    let instances = M.FloatingActionButton.init(elems);
  }
}
