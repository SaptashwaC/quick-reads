import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { StoriesModule } from './stories/stories.module';
import { ProfileModule } from './profile/profile.module';


const routes: Routes = [
  {
    path:'', 
    component:DashboardComponent,
    children: [
      {
        path:'stories', loadChildren : ()=> StoriesModule
      },
      {
        path:'profile', loadChildren : ()=> ProfileModule
      },
    ]
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
