import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  showFiller = false;
  @Output() toggle: EventEmitter<boolean> = new EventEmitter();
  routedState: string= '';
  isLoading: boolean = false;
  

  constructor(private _router: Router, private activatedRoute: ActivatedRoute) {
    _router.events.subscribe((val) => {
     
      if (val instanceof NavigationEnd) {
         console.log(val.url)
         this.routedState = val.url;
      }
    });
   }

  ngOnInit(): void {
  }

  toggleDrawer = () => {
    this.toggle.emit();
  };

  toggleRoute = (destination: string) => {
    this._router.navigate([destination]);
  }

}
