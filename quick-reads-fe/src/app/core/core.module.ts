import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreMaterialModule } from './core-material.module';
import { HeaderComponent } from './header/header.component';
import { FlexLayoutModule } from '@angular/flex-layout';
 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { GlobalSearchComponent } from './global-search/global-search.component';
import { LoaderComponent } from './loader/loader.component';
import { HeaderActionComponent } from './header-actions/header-action.component';



@NgModule({
  declarations: [
    HeaderComponent,
    GlobalSearchComponent,
    LoaderComponent,
    HeaderActionComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // BrowserModule,
    // BrowserAnimationsModule,

    CoreMaterialModule,
    FlexLayoutModule
  ],
  exports: [FormsModule, ReactiveFormsModule, HttpClientModule, HeaderComponent, CoreMaterialModule, FlexLayoutModule, GlobalSearchComponent],
})
export class CoreModule { }
