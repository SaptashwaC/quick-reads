import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class GlobalSearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
