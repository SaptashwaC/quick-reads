import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { LoginModule } from './pages/welcome/login/login.module';
import { WelcomeModule } from './pages/welcome/welcome.module';

const routes: Routes = [
  {path:'',
  redirectTo:'/login',
  pathMatch:'full'
  },
  
  {
  path : "",
  loadChildren : ()=> WelcomeModule
  },
  {
    path : "user",
    loadChildren : ()=> DashboardModule
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
