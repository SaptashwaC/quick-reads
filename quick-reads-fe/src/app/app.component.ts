import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SidenavSettingsConst } from './shared/constants/settings';
import { SideNavSettings } from './shared/interfaces/sidenav-settings.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnChanges {
  title = 'quick-reads-fe';
  sideNavSettings: SideNavSettings = {mode: SidenavSettingsConst.DRAWER_MODE_VALUE, hasBackDrop: SidenavSettingsConst.DRAWER_BACKDROP_VALUE};
  constructor(private router: Router) {

  }
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }
}
