import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { CarouselComponent } from './components/carousel/carousel.component';
import { SwiperModule } from 'swiper/angular';
import { FloatingCreateButtonComponent } from './components/floating-create-button/floating-create-button.component';




@NgModule({
  declarations: [
    CarouselComponent,
    FloatingCreateButtonComponent,
  ],
  imports: [
    CoreModule,
    CommonModule,
    SwiperModule

  ],
  exports: [CarouselComponent, FloatingCreateButtonComponent]
})
export class SharedModule { }
