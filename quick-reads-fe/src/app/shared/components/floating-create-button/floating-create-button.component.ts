import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as M from 'materialize-css';
import { TooltipSettings } from '../../constants/settings';


@Component({
  selector: 'app-floating-create-button',
  templateUrl: './floating-create-button.component.html',
  styleUrls: ['./floating-create-button.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class FloatingCreateButtonComponent implements OnInit {

  position = {value: TooltipSettings.LEFT}
  constructor() { }
  settings: Partial<M.FloatingActionButtonOptions> = {
    direction: 'top'
  };

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    // no errors
    var elems = document.querySelectorAll('.fixed-action-btn');
    let instances = M.FloatingActionButton.init(elems, this.settings);
  }
}
