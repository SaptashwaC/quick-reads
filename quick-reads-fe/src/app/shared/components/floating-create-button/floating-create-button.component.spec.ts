import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatingCreateButtonComponent } from './floating-create-button.component';

describe('FloatingCreateButtonComponent', () => {
  let component: FloatingCreateButtonComponent;
  let fixture: ComponentFixture<FloatingCreateButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloatingCreateButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingCreateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
