export class SidenavSettingsConst {
    public static DRAWER_MODE_VALUE = 'side';
    public static DRAWER_BACKDROP_VALUE = 'false'
}

export const TooltipSettings = {
    LEFT: 'left',
    AFTER: 'after',
    BEFORE: 'before',
    ABOVE: 'above',
    RIGHT: 'right',
    BELOW: 'below',

} 